<?php
	
	require_once "Zend/Loader.php";
	Zend_Loader::loadClass("Zend_Soap_Client");
	
	$opcije = array(
		"location" => "http://vezba1.nawp.ict/index/soap",
		"uri" => "http://vezba1.nawp.ict/index/soap"
	);
	
	try {
	
		$client = new Zend_Soap_Client(null, $opcije);
		$lista = $client->getGalerije();
		
		print_r($lista);
		
		// create gallery
		$data = array(
			"naziv" => "soapGalerija"
		);
		//$client->addGalerija($data);
	
	} catch(Bloger_Exception $ex) {
		echo "ERROR: ".$ex->getMessage();
	} catch(SoapFault $ex) {
		echo "ERROR[".$ex->faultcode."] ".$ex->faultstring;
	} catch(Exception $ex) {
		echo "ERROR: ".$ex->getMessage();
	}
	
	//https://manojlovic@bitbucket.org/manojlovic/ict_spec_zend.git
?>
