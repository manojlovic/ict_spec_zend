<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Manager
 *
 * @author php
 */
class Bloger_Manager {

    /**
     * Dodaje novu galeriju u bazu podataka
     *
     * @param array $data niz podataka tako da indeks -> ime polja u tabeli
     * @return integer id poslednje unete galerije
     */
    public function addGalerija($data) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $db->insert('galerija', $data);
        return $db->lastInsertId();
    }

    /**
     * Vraca listu postova iz baze podataka
     *
     * @return array
     */
    public function getGalerije() {
        try {
            // moze i preko modela samo u trenutku pisanja nismo kreirali model
            $db = Zend_Db_Table::getDefaultAdapter();
            $sql = "SELECT * FROM galerija";
            return $db->fetchAll($sql);
        } catch (Bloger_Exception $ex) {
            throw $ex;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Vraca odredjeni post iz baze podataka
     *
     * @param integer $id
     * @return array|Exception
     */
    public function getGalerija($id) {
        if (!Zend_Validate::is($id, 'Int')) {
            throw new Bloger_Exception('Neipravan podatak');
        }
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT * FROM galerija WHERE idGalerija = '$id'";
        $result = $db->fetchAll($sql);
        if (count($result) != 1) {
            throw new Exception('Neispravan ID: ' . $id);
        }
        return $result;
    }

}
