<?php

class Application_Form_Slika extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');
        $imeSlike = new Zend_Form_Element_Text('tbNaziv');
        $imeSlike->setAttrib('class', 'text-box');
        $imeSlike->setLabel('Naziv');
        $imeSlike->setRequired(TRUE);
        $imeSlike->addValidator('NotEmpty');
        $imeSlike->addFilter('HtmlEntities')->addFilter('StringTrim');

        $this->addElement($imeSlike);

        $slika = new Zend_Form_Element_File('slika');
        $slika->setLabel('Slika')->setRequired('TRUE')->addValidator('NotEmpty')->addFilter('StringTrim');

        $this->addElement($slika);
        $galerijeMapper = new Application_Model_GalerijaMapper();
        $galerije = $galerijeMapper->fetchAll(null);

        $listaGalerija = new Zend_Form_Element_Select('ddlGalerija');
        $listaGalerija->setAttrib('class', 'drop-down-list');
        $listaGalerija->setAttrib('id', 'ddlGalerija');
        $listaGalerija->setLabel('Galerija')->setRequired(TRUE);

        foreach ($galerije as $galerija) {
            $listaGalerija->addMultiOption($galerija->getId(), $galerija->getNaziv());
        }

        $this->addElement($listaGalerija);

        $submit = new Zend_Form_Element_Submit('btnSubmit');
        $submit->setLabel('Unesi');
        $submit->setAttrib('class', 'button');
        $this->addElement($submit);
    }


}

