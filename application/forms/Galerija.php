<?php

class Application_Form_Galerija extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        $this->setAction('/Administracija/galerija');
        $this->setMethod('post');
        
        $tbNaziv=new Zend_Form_Element_Text('tbNaziv');
        $tbNaziv->addFilter('StringTrim');
        $tbNaziv->setRequired(true);
        $tbNaziv->addValidator('NotEmpty');
        $tbNaziv->setLabel('Naziv');
        
        $btnSubmit=new Zend_Form_Element_Submit('btnSubmit');
        $btnSubmit->setLabel('Unesi');
        
        $this->addElement($tbNaziv);
        $this->addElement($btnSubmit);
        
    }


}

