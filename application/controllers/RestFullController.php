<?php

class RestFullController extends Zend_Rest_Controller
{
    private $_vesti;
    private $_api_key;
    
    public function init()
    {
        $this->_helper->layout()->disableLayout();
        $this->getHelper("viewRenderer")->setNoRender(true);
        $this->_api_key = "1234";
        $this->_vesti = array(
            "1" => "Vest 1",
            "2" => "Vest 2",
            "3" => "Vest 3"
        );
    }

    public function indexAction()
    {
        // action body
    }
    
    /**
     *  Set by default body is empty
     */
    public function headAction() {
        $this->getResponse()->setBody(NULL);
    }

    public function getAction() {
        $request = $this->getRequest();
        $id = $request->getParam("id");
        
        $api_key = $request->getParam("api_key");
        if( $api_key == $this->_api_key ) {
            $json = Zend_Json::encode($this->_vesti);
            
            $this->getResponse()->setHeader("Content-Type", "application/json")
                    ->appendBody($json);
            
            $this->getResponse()->setHttpResponseCode(200);
        } else {
            $this->getResponse()->setHttpResponseCode(403);
        }
//        $this->getResponse()->setHttpResponseCode(200);
//        $this->getResponse()->setBody("Zdravo GET: id=$id<br/>");
    }
    
    public function postAction() {
        $request = $this->getRequest();
        $id = $request->getParam("id");
        
        $this->getResponse()->setHttpResponseCode(200);
        $this->getResponse()->setBody("Zdravo POST: id=$id<br/>");
    }
    
    public function putAction() {
        $request = $this->getRequest();
        $id = $request->getParam("id");
        
        $this->getResponse()->setHttpResponseCode(200);
        $this->getResponse()->setBody("Zdravo PUT: id=$id<br/>");
    }
    
    public function deleteAction() {
        $request = $this->getRequest();
        $id = $request->getParam("id");
        
        $this->getResponse()->setHttpResponseCode(200);
        $this->getResponse()->setBody("Zdravo DELETE: id=$id<br/>");
    }
}

