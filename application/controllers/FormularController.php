<?php

class FormularController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $loginForm = new Zend_Form();
        $loginForm->setAction('/Formular/index')->setMethod('post');
        $loginForm->setAttrib('id', 'loginForma');
        $loginForm->setAttrib('name', 'loginForma');
        //$loginForm->setDecorators(array('FormElements'));

        $username = new Zend_Form_Element_Text('tbUsername');
        $username->setLabel('Korisnicko ime:');
        $username->setRequired(true);
        $username->addValidator('regex', false, array('/^[a-z]*$/'))->addErrorMessage('Korisnicko ime nije u dobrom formatu !!!!');
        $username->setAttrib('id', 'tbUsername');
        $username->addFilter('StringToLower');
        $username->setDescription('Unesite korisnicko ime');

        $password = new Zend_Form_Element_Password('tbLozinka');
        $password->setLabel('Lozinka:');
        $password->setRequired(true);
        $password->addValidator('regex', false, array('/^[a-z]*$/'));
        $password->setAttrib('id', 'tbLozinka');


        $submit = new Zend_Form_Element_Submit('btnLogin');
        $submit->setLabel('Login');
        $submit->setAttrib('id', 'btnSubmit');

        $loginForm->addElement($username);
        $loginForm->addElement($password);
        $loginForm->addElement($submit);

        $loginForm->setElementDecorators(array(
            'ViewHelper',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
            array('Label', array('tag' => 'td')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
        ));

        $submit->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
            array(array('emptyrow' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element', 'placement' => 'PREPEND')),
            array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
        ));

        $loginForm->setDecorators(array('FormElements',
            array('HtmlTag', array('tag' => 'table')),
            'Form',
            array('Fieldset', array('legend' => 'Login'))
        ));

        $request = $this->getRequest();
        if ($request->isPost() && $loginForm->isValid($request->getPost())) {
            $podaci = $loginForm->getValues();
            $korisnickoIme = $podaci['tbUsername'];
            $lozinka = md5($podaci['tbLozinka']);

            $auth = Zend_Auth::getInstance();
            $korisnikTabela = new Application_Model_DbTable_Korisnik();
            $authAdapter = new Zend_Auth_Adapter_DbTable($korisnikTabela->getAdapter(), 'korisnik');
            $authAdapter->setIdentityColumn('korisnickoIme')->setCredentialColumn('lozinka');
            $authAdapter->setIdentity($korisnickoIme)->setCredential($lozinka);

            $result = $auth->authenticate($authAdapter);

            if ($result->isValid()) {
                $sesija = new Zend_Auth_Storage_Session();
                $data = $authAdapter->getResultRowObject(null, 'lozinka'); // izvici sve podatke iz tabele osim lozinke

                $korisnikMapper = new Application_Model_KorisnikMapper();
                $registrovaniKorisnik = new Application_Model_Korisnik();

                $korisnikMapper->find($data->idKorisnik, $registrovaniKorisnik); // nadji i smesiti u registrovaniKorisnik

                $sesija->write($registrovaniKorisnik);
                
                if ($registrovaniKorisnik->getUloga()->naziv == 'Administrator') { // autorizacija
                    $this->_redirect('/Administracija');
                } else {
                    $this->_redirect('/Index');
                }
            } else {
                $layout = $this->_helper->layout();
                $layout->message = 'Podaci za logovanje nisu ispravni.';
            }
        }

        $this->view->forma = $loginForm;
    }

}
