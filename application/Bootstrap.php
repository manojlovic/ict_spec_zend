<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initDoctype() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
    }

    protected function _initPlaceholders() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        
        $view->headLink()->appendStylesheet('/style.css');
        $view->headScript()->appendFile('/js/test.js');
        $view->headTitle('Napredno Web Programiranje')->setSeparator("::");
    }
    
    protected function _initSidebar() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        
        $view->placeholder("sidebar")
                ->setPrefix("<ul>\n<li>")
                ->setSeparator("</li>\n<li>")
                ->setPostfix("</li>\n</ul>");
    }
    
    public function _initRoutes() {
        $front = Zend_Controller_Front::getInstance();

        $router = $front->getRouter();
        //navodite da su svi kontroleri RestFull
        $restRoute = new Zend_Rest_Route($front);
//        $router->addRoute('default', $restRoute);
    }
}

