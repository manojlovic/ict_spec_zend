<?php

class Application_Model_KorisnikMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception("Nepostojeci table geteway");
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null == $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Korisnik');
        }

        return $this->_dbTable;
    }

    public function save(Application_Model_Korisnik $korisnik) {

        $data = array(
            'idKorisnik' => $korisnik->getId(),
            'korisnickoIme' => $korisnik->getKorisnickoIme(),
            'lozinka' => md5($korisnik->getLozinka()),
            'email' => $korisnik->getEmail(),
            'aktivan' => $korisnik->getStatus(),
            'datum' => time(),
            'idUloga' => $korisnik->ulogaId()
        );
        if ($korisnik->getLozinka() == '') {
            unset($data['lozinka']);
        }
        if (null === ($id = $korisnik->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('idKorisnik=?' => $id));
        }
    }

    public function find($id, Application_Model_Korisnik $korisnik) {
        $result = $this->getDbTable()->find($id);
        if (count($result) == 0) {
            return;
        }
        $row = $result->current();
        $korisnik->setId($row->idKorisnik)->setKorsnickoIme($row->korisnickoIme)->setLozinka($row->lozinka)->setEmail($row->email)->setStatus($row->aktivan)->setDatum($row->datum)->setUloga($row->findParentRow('Application_Model_DbTable_Uloga'));
    }

    public function fetchAll() {
        $resultSet = $this->getDbTable()->fetchAll();

        $korisnici = array();

        foreach ($resultSet as $row) {
            $korisnik = new Application_Model_Korisnik();
            $korisnik->setId($row->idKorisnik)->setKorsnickoIme($row->korisnickoIme)->setLozinka($row->lozinka)->setEmail($row->email)->setStatus($row->aktivan)->setDatum($row->datum)->setUloga($row->findParentRow('Application_Model_DbTable_Uloga'));
            //findManyToManyRowset('Application_Model_DbTable_Uloge','Application_Model_DbTable_KorisnikUloga','Korisnik')
            $korisnici[] = $korisnik;
        }

        return $korisnici;
    }

    public function delete($id) {
        $where = $this->getDbTable()->getAdapter()->quoteInto('idKorisnik=?', $id);
        $this->getDbTable()->delete($where);
    }

}
