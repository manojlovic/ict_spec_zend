<?php

class Application_Model_Korisnik {

    protected $_id;
    protected $_korinsnickoIme;
    protected $_lozinka;
    protected $_mail;
    protected $_datum;
    protected $_status;
    protected $_uloga;

    //zakomentarisano jer nisu implementirane veze
    //protected $_komentari;
    //protected $_stranice;

    public function _set($name, $value) {

        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Svojstvo za Korisnik nije definisano');
        }
        $this->$method($value);
    }

    public function _get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Svojstvo za Korisnik nije definisano');
        }
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setKorsnickoIme($text) {
        $this->_korinsnickoIme = $text;
        return $this;
    }

    public function getKorisnickoIme() {
        return $this->_korinsnickoIme;
    }

    public function setLozinka($text) {
        $this->_lozinka = $text;
        return $this;
    }

    public function getLozinka() {
        return $this->_lozinka;
    }

    public function setEmail($text) {
        $this->_mail = $text;
        return $this;
    }

    public function getEmail() {
        return $this->_mail;
    }

    public function setDatum($val) {
        $this->_datum = $val;
        return $this;
    }

    public function getDatum() {
        return $this->_datum;
    }

    public function setStatus($val) {
        $this->_status = $val;
        return $this;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function setUloga($uloga) {
        $this->_uloga = $uloga;
        return $this;
    }

    public function getUloga() {

        return $this->_uloga;
    }
    
    public function nazivUloga() {
        if(isset($this->_uloga)) {
            return $this->_uloga['naziv'];
        }
    }

}
