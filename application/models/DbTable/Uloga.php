<?php

class Application_Model_DbTable_Uloga extends Zend_Db_Table_Abstract {

    protected $_name = 'uloga';
    protected $_id = 'idUloga';
    protected $_dependentTables = array('Application_Model_DbTable_Korisnik');

}
