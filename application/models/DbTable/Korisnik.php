<?php

class Application_Model_DbTable_Korisnik extends Zend_Db_Table_Abstract {

    protected $_name = 'korisnik';
    protected $_id = 'idKorisnik';
    protected $_referenceMap = array(
        'Uloga' => array(
            'columns' => array('idUloga'),
            'refTableClass' => 'Application_Model_DbTable_Uloga',
            'refColumns' => array('idUloga')
        ),
    );

    //tabele u kojima je idKorisnik strani kljuc, zakomentarisano je iz razloga sto te tabele nisu implementirane 
    //protected $_dependentTables=array('Application_Model_DbTable_Post','Application_Model_DbTable_Komentar');
}
