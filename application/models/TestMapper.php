<?php

class Application_Model_TestMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception("Nepostojeci table gateway");
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null == $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Test');
        }

        return $this->_dbTable;
    }

    public function save(Application_Model_Test $test) {
        $data = array(
            'text' => $test->getText()
        );

        if (null === ($id = $test->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id=?' => $id));
        }
    }

    public function find($id, Application_Model_Slika $test) {
        $result = $this->getDbTable()->find($id);
        if (count($result) == 0) {
            return;
        }
        $row = $result->current();
        $test->setId($row->id)->setText($row->text);
    }

    public function fetchAll() {
        $resultSet = $this->getDbTable()->fetchAll();
        $tests = array();

        foreach ($resultSet as $row) {
            $test = new Application_Model_Test();
            $test->setId($row->id)->setText($row->text);

            $tests[] = $test;
        }

        return $tests;
    }

}
