<?php

//class Application_Model_Galerija
//{
//    protected $db;
//    protected $id;
//    protected $naziv;
//    
//    function __construct() {
//        try {
//            $this->db = Zend_Db_Table::getDefaultAdapter();
//        } catch (Exception $ex) {
//            throw $ex;
//        }
//    }
//
//    public function fetchAll() {
////        $result = $this->db->query("SELECT * FROM galerija");
//        $select = $this->db->select()->from("galerija");
//        $result = $this->db->query($select);
//        
//        return $result;
//    }
//
//    public function fetch() {
//        $select = $this->db->select()->from("galerija")->where("idGalerija=?", $this->id);
//        $result = $this->db->query($select)->fetchObject();
//        
//        return $result;
//    }
//    
//    public function insert() {
//        $podaci = array(
//            "naziv" => $this->naziv
//        );
//        
//        $this->db->insert("galerija", $podaci);
//    }
//    
//    public function update() {
//        $podaci = array(
//            "naziv" => $this->naziv
//        );
//        
//        $where["idGalerija=?"] = $this->id;
//        
//        try {
//            $this->db->update("galerija", $podaci, $where);
//        } catch (Exception $ex) {
//            throw $ex;
//        }
//    }
//    
//    public function delete() {
//        try {
//            $this->db->delete("galerija", "idGalerija=?". $this->db);
//        } catch (Exception $ex) {
//            throw $ex;
//        }
//    }
//            
//    function getId() {
//        return $this->id;
//    }
//
//    function getNaziv() {
//        return $this->naziv;
//    }
//
//    function setId($id) {
//        $this->id = $id;
//    }
//
//    function setNaziv($naziv) {
//        $this->naziv = $naziv;
//    }
//
//}

class Application_Model_Galerija {

    public $_id;
    public $_naziv;
    public $_slike;

    public function __construct(array $options = null) {

        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function _set($name, $value) {

        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Svojstvo za Galeriju nije definisano');
        }
        $this->$method($value);
    }

    public function _get($name, $value) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Svojstvo za Geleriju nije definisano');
        }
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setNaziv($text) {
        $this->_naziv = $text;
        return $this;
    }

    public function getNaziv() {
        return $this->_naziv;
    }

    public function getSlike() {
        return $this->_slike;
    }

    public function setSlike($slike) {
        $this->_slike = $slike;
        return $this;
    }

}
