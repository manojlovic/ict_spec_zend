<?php

class Application_Model_SlikaMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception("Nepostojeci table gateway");
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null == $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Slika');
        }

        return $this->_dbTable;
    }

    public function save(Application_Model_Slika $slika) {
        $data = array(
            'naziv' => $slika->getNaziv(),
            'putanja' => $slika->getPutanja(),
            'velicina' => $slika->getVelicina(),
            'glavna' => $slika->getGlavna()
        );

        if (null === ($id = $slika->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('idSlika=?' => $id));
        }
    }

    public function find($id, Application_Model_Slika $slika) {
        $result = $this->getDbTable()->find($id);
        if (count($result) == 0) {
            return;
        }
        $row = $result->current();
        $slika->setId($row->idSlika)->setNaziv($row->naziv)->setPutanja($row->putanja)->setVelicina($row->velicina)->setGlavna($row->glavna)->setGalerija($row->findParentRow('Application_Model_DbTable_Galerija'));
    }

    public function fetchAll() {
        $resultSet = $this->getDbTable()->fetchAll();
        $slike = array();

        foreach ($resultSet as $row) {
            $slika = new Application_Model_Slika();
            $slika->setId($row->idSlika)->setNaziv($row->naziv)->setPutanja($row->putanja)->setVelicina($row->velicina)->setGlavna($row->glavna)->setGalerija($row->findParentRow('Application_Model_DbTable_Galerija'));

            $slike[] = $slika;
        }

        return $slike;
    }

}
