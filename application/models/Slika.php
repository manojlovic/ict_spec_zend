<?php

class Application_Model_Slika {

    protected $_id;
    protected $_naziv;
    protected $_putanja;
    protected $_velicina;
    protected $_glavna;
    protected $_galerija;

    public function _set($name, $value) {

        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Svojstvo za Sliku nije definisano');
        }
        $this->$method($value);
    }

    public function _get($name, $value) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Svojstvo za Sliku nije definisano');
        }
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setNaziv($text) {
        $this->_naziv = $text;
        return $this;
    }

    public function getNaziv() {
        return $this->_naziv;
    }

    public function setPutanja($text) {
        $this->_putanja = $text;
        return $this;
    }

    public function getPutanja() {
        return $this->_putanja;
    }

    public function setVelicina($velicina) {
        $this->_velicina = $velicina;
        return $this;
    }

    public function getVelicina() {
        return $this->_velicina;
    }

    public function setGlavna($glavna) {
        $this->_glavlna = (bool) $glavna;
        return $this;
    }

    public function getGlavna() {
        return $this->_glavna;
    }

    public function setGalerija($galerija) {
        $this->_galerija = $galerija;
        return $this;
    }

    public function getGalerija() {
        return $this->_galerija;
    }

    public function getNazivGalerije() {
        $galerija = $this->_galerija->toArray();
        return $galerija['naziv'];
    }

}
